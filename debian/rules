#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

tmp := $(CURDIR)/debian/swish++
renamed := extract index search splitmail
man4to5 := man4/swish++.conf.4  man4/swish++.index.4

install_file = install -p    -o root -g root -m 644
install_program = install -p    -o root -g root -m 755
install_script = install -p    -o root -g root -m 755
makedirectory = install -p -d -o root -g root -m 755
# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# These are probably irrelevant since the source is g++
CFLAGS = -g -Wall
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
CFLAGS += -O0
else
CFLAGS += -O2
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
install_program += -s
endif

include /usr/share/quilt/quilt.make

configure: configure-stamp

configure-stamp: $(QUILT_STAMPFN)
	dh_testdir
	## Patches and configuring
	#ln -s debian/patches
	#quilt push -a || test $$? = 2
	# Fix location of man pages
	mkdir -p $(CURDIR)/man/man5
	for i in $(man4to5); do j=$$(echo $${i} | tr '4' '5'); cp -p man/$${i} man/$${j}; done
	touch configure-stamp	

build: build-stamp

build-stamp: configure-stamp
	dh_testdir
	# Add here commands to compile the package.
	$(MAKE)
	touch build-stamp

clean: unpatch
	dh_testdir
	dh_testroot
	## Reverse patches
	#quilt pop -a || test $$? = 2
	## Clean up quilt stuff
	#rm -rf patches .pc
	#rm -f build-stamp configure-stamp
	# Add here commands to clean up after the build process.
	$(MAKE) distclean
	# Clean up a left over file
	rm -f init_mod_vars.c
	# Clean up the renamed man pages
	for i in $(renamed); do rm -f $(CURDIR)/man/man1/$${i}++.1; done;
	# Remove copied manpages
	rm -rf $(CURDIR)/man/man5
	rm -f build-stamp configure-stamp
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
#	$(MAKE)  install DESTDIR=$(tmp)
	$(makedirectory) $(tmp)/usr/lib/swish++
	$(makedirectory) $(tmp)/usr/bin
	$(install_file) WWW.pm $(tmp)/usr/lib/swish++/
	$(install_program) search $(tmp)/usr/bin/search++
	$(install_program) extract $(tmp)/usr/bin/extract++
	$(install_program) index $(tmp)/usr/bin/index++
	$(install_script) scripts/splitmail $(tmp)/usr/bin/splitmail++
	$(install_script) scripts/httpindex $(tmp)/usr/bin/httpindex

	cd $(CURDIR)/man/man1 && for i in $(renamed); do cp -p $${i}.1 $${i}++.1; done;

	$(makedirectory) $(CURDIR)/debian/daemon
	$(install_script) scripts/searchmonitor $(CURDIR)/debian/daemon/searchmonitor
	$(install_script) scripts/searchc $(CURDIR)/debian/daemon/searchc
	$(install_script) scripts/searchd $(CURDIR)/debian/daemon/searchd
	# should customize the whole install as it's really opaque now


# Build architecture-independent files here.
binary-indep: build install
# We have nothing to do by default.

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
#	dh_installdebconf	
	dh_installdocs
	dh_installexamples $(CURDIR)/debian/email_indexing/ \
		$(CURDIR)/debian/oo_indexing $(CURDIR)/debian/daemon/ \
		$(CURDIR)/swish++.conf  $(CURDIR)/www_example/ \
		$(CURDIR)/debian/rlimit
	dh_installmenu
#	dh_installlogrotate
#	dh_installemacsen
#	dh_installpam
#	dh_installmime
#	dh_installinit
	dh_installcron
	dh_installman man/man1/splitmail++.1 man/man1/search++.1 man/man1/extract++.1 man/man1/index++.1 \
	man/man1/httpindex.1 man/man3/WWW.3 man/man5/swish++.conf.5 man/man5/swish++.index.5
	dh_installinfo
	dh_installchangelogs Changes
	dh_link
	dh_strip
	dh_compress
	dh_fixperms
#	dh_makeshlibs
	dh_installdeb
#	dh_perl
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install configure
