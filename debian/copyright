Authors
=======

This package was debianized by Jim Pick <jim@jimpick.com> on
Fri, 13 Mar 1998 22:41:24 -0800.

It has since then been maintained by Josip Rodin
<jrodin@jagor.srce.hr> and Michael Hummel <mhummel@debian.org>.

The current package is maintained by Kapil Hari Paranjape
<kapil@imsc.res.in>.

In addition to the Debian Maintainers and the Upstream author the
files in the debian/ directory have been copyrighted by
Joey Hess <joeyh@debian.org>,
Kai Gro<DF>johann <grossjohann@ls6.cs.uni-dortmund.de>,
Christoph Conrad <christoph.conrad@gmx.de> and
Simon Josefsson <jas@pdc.kth.se>.

The files in debian/oo_indexing are copyrighted by
Bastian Kleineidam <calvin@debian.org>.

The original source was found at
        http://homepage.mac.com/pauljlucas/software/swish/

The current source was downloaded from
	http://swishplusplus.sourceforge.net/

Upstream Author: Paul J. Lucas <pauljlucas@mac.com>.

Copyright
=========

      Copyright (C) 1998-2006  Paul J. Lucas
      Copyright (C) 1997-1999  Joey Hess
      Copyright (C) 1998       Kai Gro<DF>johann
      Copyright (C) 2000-2001  Christoph Conrad
      Copyright (C) 1998, 2000-2001  Simon Josefsson

All the source files used to build the Debian package contain
copyright statements like the above along with text like the
following from index.c:

      This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License as published by
      the Free Software Foundation; either version 2 of the License, or
      (at your option) any later version.
 
      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

On the Debian system, the full text of the GNU General Public License can be
found in the file `/usr/share/common-licenses/GPL'.

OpenOffice Indexing Example files:

The OpenOffice Indexing Example files are by Bastian Kleineidam <calvin@debian.org>
They contain copyright statements like the following taken from
oo.conf:

	Copyright (C) 2004 Bastian Kleineidam <calvin@debian.org>
	The contents of this file are released in the Public Domain


Additional Files:

The files fnmatch.[ch] in the source package are used only in the
Win32 build. These are copyright DJ Delorie and contain the copyright
notice:

	Copyright (C) 1995 DJ Delorie, see COPYING.DJ for details

The file copying.dj is reproduced here in the interest of having all
copyright statements related to the source in one place. It reads:

========copying.dj==============
This is the file "copying.dj".  It does NOT apply to any sources or
binaries copyrighted by UCB Berkeley, the Free Software Foundation, or
any other agency besides DJ Delorie and others who have agreed to
allow their sources to be distributed under these terms.

   Copyright Information for sources and executables that are marked
   Copyright (C) DJ Delorie
                 7 Kim Lane
                 Rochester NH  03867-2954

This document is Copyright (C) DJ Delorie and may be distributed
verbatim, but changing it is not allowed.

Source code copyright DJ Delorie is distributed under the terms of the
GNU General Public Licence, with the following exceptions:

* Sources used to build crt0.o, gcrt0.o, libc.a, libdbg.a, and
  libemu.a are distributed under the terms of the GNU Library General
  Public License, rather than the GNU GPL.

* Any existing copyright or authorship information in any given source
  file must remain intact.  If you modify a source file, a notice to that
  effect must be added to the authorship information in the source file. 

* Runtime binaries, as provided by DJ in DJGPP, may be distributed
  without sources ONLY if the recipient is given sufficient information
  to obtain a copy of djgpp themselves.  This primarily applies to
  go32-v2.exe, emu387.dxe, and stubedit.exe.

* Runtime objects and libraries, as provided by DJ in DJGPP, when
  linked into an application, may be distributed without sources ONLY
  if the recipient is given sufficient information to obtain a copy of
  djgpp themselves.  This primarily applies to crt0.o and libc.a.

-----

Changes to source code copyright BSD or FSF by DJ Delorie fall under
the terms of the original copyright.

A copy of the files "COPYING" and "COPYING.LIB" are included with this
document.  If you did not receive a copy of these files, you may
obtain one from whence this document was obtained, or by writing.

[ At this point copying.dj refers to the old FSF address  ]

========end of copying.dj==============

The file COPYING is the GPL text.
On Debian system, the full text of the GNU General Public
License can be found in the file `/usr/share/common-licenses/GPL'.

The file COPYING.LIB is the Library GPL text.
On Debian system, the full text of the Library GNU General Public
License can be found in the file `/usr/share/common-licenses/LGPL-2'.

